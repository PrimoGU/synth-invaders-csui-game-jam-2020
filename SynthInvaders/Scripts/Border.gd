extends StaticBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Top_body_entered(body):
	if Global.player != null:
		if body.is_in_group("Players"):
			Global.player.velocity.y = -(Global.player.velocity.y)

func _on_Bottom_body_entered(body):
	if Global.player != null:
		if body.is_in_group("Players"):
			Global.player.velocity.y = -(Global.player.velocity.y)

func _on_Right_body_entered(body):
	if Global.player != null:
		if body.is_in_group("Players"):
			Global.player.velocity.x = -(Global.player.velocity.x)

func _on_Left_body_entered(body):
	if Global.player != null:
		if body.is_in_group("Players"):
			Global.player.velocity.x = -(Global.player.velocity.x)
