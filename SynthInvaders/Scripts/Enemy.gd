extends KinematicBody2D

export (int) var speed = rand_range(60,120)
export (int) var rot_speed = rand_range (-5,5)

var spawned = false
var velocity = Vector2()

var shatter_particles = preload("res://Scenes/Enemy Blood.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	$SpriteShadow.position.x = rand_range(-20,20)
	$SpriteShadow.position.y = rand_range(-20,20)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if Global.player != null and spawned == false:
		velocity = self.global_position.direction_to(Global.player.global_position)
		spawned = true
	
	self.global_position += velocity * speed * delta
	
	if (self.rotation_degrees + rot_speed) >= 360:
		self.rotation_degrees -= 360
	if (self.rotation_degrees + rot_speed) <= -360:
		self.rotation_degrees += 360
	
	self.rotation_degrees += rot_speed
	check_location()
	
func check_location():
	if self.global_position.x > 1300 or self.global_position.x < -150:
		if self.global_position.y > 800 or self.global_position.y < -150:
			destroy()

func destroy():
	$Sprite.visible = false
	$SpriteShadow.visible = false
	
	if Global.node_creation_parent != null:
		Global.instance_node(shatter_particles, self.global_position, Global.node_creation_parent)
	queue_free()
