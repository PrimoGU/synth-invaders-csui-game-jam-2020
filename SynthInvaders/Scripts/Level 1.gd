extends Node2D

var enemy_1 = preload("res://Scenes/Enemy.tscn")
var boss_1 = preload("res://Scenes/Boss 1.tscn")
var low_note_1 = preload("res://Scenes/Low Note.tscn")
var high_note_1 = preload("res://Scenes/High Note.tscn")
var shield_1 = preload("res://Scenes/Shield Item.tscn")
var health_1 = preload("res://Scenes/Health Item.tscn")

var start = false

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.node_creation_parent = self
	Global.current_level = "Level 1"
	intro()
	
func _exit_tree():
	Global.node_creation_parent = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass
	
func intro():
	var note_pos = Vector2(800,575)
	
	Global.instance_node(low_note_1, note_pos, self)
	yield(get_tree().create_timer(.75), "timeout")
	
	Global.instance_node(low_note_1, note_pos, self)
	yield(get_tree().create_timer(.75), "timeout")
	
	$LowNoteTimer.start()
	Global.instance_node(low_note_1, note_pos, self)
	yield(get_tree().create_timer(.375), "timeout")
	
	$ChordTimer.start()
	Global.instance_node(low_note_1, note_pos, self)
	yield(get_tree().create_timer(.375), "timeout")
	
	$HighNoteTimer.start()
	Global.instance_node(low_note_1, note_pos, self)
	yield(get_tree().create_timer(.375), "timeout")
	
	Global.instance_node(low_note_1, note_pos, self)
	
func start_level():
	if start == false:
		start = true
		$BossSpawnTimer.start()
		
func win_screen():
	$WinLabel.visible = true
	yield(get_tree().create_timer(5), "timeout")
	var scene_to_load = "Level 2"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
func lose_screen():
	$LoseLabel.visible = true
	yield(get_tree().create_timer(3), "timeout")
	var scene_to_load = "Game Over"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_EnemySpawnTimer_timeout():
	var enemy_position = Vector2(rand_range(-100,1250), rand_range(-100,750))
	
	while (enemy_position.x < 1152 and enemy_position.x > 0) or (enemy_position.y < 648 and enemy_position.y > 0):
		enemy_position = Vector2(rand_range(-100,1250), rand_range(-100,750))
		
	Global.instance_node(enemy_1, enemy_position, self)

func _on_LowNoteTimer_timeout():
	var low_pos = Vector2(800,575)
	if Global.player != null:
		Global.instance_node(low_note_1, low_pos, self)

func _on_HighNoteTimer_timeout():
	var high_pos = Vector2(800,520)
	if Global.player != null:
		Global.instance_node(high_note_1, high_pos, self)

func _on_PowerSpawnTimer_timeout():
	var powerup = randi() % 2
	var power_pos = Vector2(rand_range(10,1000), rand_range(40,500))
	
	if Global.player != null:
		if powerup == 0:
			Global.instance_node(shield_1, power_pos, self)
		if powerup == 1:
			Global.instance_node(health_1, power_pos, self)

func _on_ChordTimer_timeout():
	var low_pos = Vector2(800,575)
	if Global.player != null:
		Global.instance_node(low_note_1, low_pos, self)

func _on_BossSpawnTimer_timeout():
	var boss_pos = Vector2(1200, 200)
	if Global.node_creation_parent == self:
		Global.instance_node(boss_1, boss_pos, self)
	$BossAudioPlayer.play()
