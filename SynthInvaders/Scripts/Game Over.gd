extends MarginContainer

export(String) var scene_to_load = null

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.node_creation_parent = self
	$AudioStreamPlayer2D.play()

func _exit_tree():
	Global.node_creation_parent = null
	$AudioStreamPlayer2D.stop()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_RetryLabel_pressed():
	scene_to_load = Global.current_level
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_MenuLabel_pressed():
	scene_to_load = "Main Menu"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
