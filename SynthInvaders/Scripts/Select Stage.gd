extends MarginContainer

export(String) var scene_to_load = null

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.node_creation_parent = self
	$AudioStreamPlayer2D.play()

func _exit_tree():
	Global.node_creation_parent = null
	$AudioStreamPlayer2D.stop()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Stage1_pressed():
	scene_to_load = "Level 1"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Stage2_pressed():
	scene_to_load = "Level 2"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_Stage3_pressed():
	scene_to_load = "Level 3"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_BackButton_pressed():
	scene_to_load = "Main Menu"
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
