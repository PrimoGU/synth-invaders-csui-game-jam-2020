extends KinematicBody2D

var speed = rand_range(50,80)
var rot_speed = rand_range (-2,2)

var speed_x = 100
var boss_lives = 10

var going_down = true
var velocity = Vector2(0,0)

var death_particles = preload("res://Scenes/Boss Blood.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.boss = self
	if Global.current_level == "Level 1":
		boss_lives = 10
	if Global.current_level == "Level 2":
		boss_lives = 15
	if Global.current_level == "Level 3":
		boss_lives = 20
	
func _exit_tree():
	Global.boss = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):	
	check_direction()
	check_lives()
	
	if going_down == true:
		self.global_position.y += speed * delta
	else:
		self.global_position.y -= speed * delta
	
	if (self.rotation_degrees + rot_speed) >= 360:
		self.rotation_degrees -= 360
	if (self.rotation_degrees + rot_speed) <= -360:
		self.rotation_degrees += 360
	
	self.global_position.x += speed_x * delta
	self.rotation_degrees += rot_speed

func check_direction():
	if self.global_position.x <= 1100:
		speed_x = 0;
	
	if self.global_position.y >= 700:
		going_down = false
	if self.global_position.y <= -50:
		going_down = true

func check_lives():		
	if boss_lives <= 0:
		$Sprite.visible = false
		$SpriteShadow.visible = false
	
		if Global.node_creation_parent != null:
			Global.node_creation_parent.win_screen()
			Global.instance_node(death_particles, self.global_position, Global.node_creation_parent)
		
		if Global.player != null:
			Global.player.invincible()
		queue_free()

func destroy():
	boss_lives -= 1
	$Sprite.modulate = Color("c8ff6464")
	yield(get_tree().create_timer(.5), "timeout")
	$Sprite.modulate = Color("ffffff")
