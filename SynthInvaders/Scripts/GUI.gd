extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$HBoxContainer/PlayerLives1.visible = false
	$HBoxContainer/PlayerLives2.visible = false
	$HBoxContainer/PlayerLives3.visible = false
	$HBoxContainer/PlayerLives4.visible = false
	$HBoxContainer/PlayerLives5.visible = false
	
	$HBoxContainer2/BossLives1.visible = false
	$HBoxContainer2/BossLives2.visible = false
	$HBoxContainer2/BossLives3.visible = false
	$HBoxContainer2/BossLives4.visible = false
	$HBoxContainer2/BossLives5.visible = false
	$HBoxContainer2/BossLives6.visible = false
	$HBoxContainer2/BossLives7.visible = false
	$HBoxContainer2/BossLives8.visible = false
	$HBoxContainer2/BossLives9.visible = false
	$HBoxContainer2/BossLives10.visible = false
	$HBoxContainer2/BossLives11.visible = false
	$HBoxContainer2/BossLives12.visible = false
	$HBoxContainer2/BossLives13.visible = false
	$HBoxContainer2/BossLives14.visible = false
	$HBoxContainer2/BossLives15.visible = false
	$HBoxContainer2/BossLives16.visible = false
	$HBoxContainer2/BossLives17.visible = false
	$HBoxContainer2/BossLives18.visible = false
	$HBoxContainer2/BossLives19.visible = false
	$HBoxContainer2/BossLives20.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.player!= null:
		check_player_lives()
	if Global.boss != null:
		check_boss_lives()
		
	if Global.player == null:
		$HBoxContainer/PlayerLives1.visible = false
		$HBoxContainer/PlayerLives2.visible = false
		$HBoxContainer/PlayerLives3.visible = false
		$HBoxContainer/PlayerLives4.visible = false
		$HBoxContainer/PlayerLives5.visible = false
	
	if Global.boss == null:
		$HBoxContainer2/BossLives1.visible = false
		$HBoxContainer2/BossLives2.visible = false
		$HBoxContainer2/BossLives3.visible = false
		$HBoxContainer2/BossLives4.visible = false
		$HBoxContainer2/BossLives5.visible = false
		$HBoxContainer2/BossLives6.visible = false
		$HBoxContainer2/BossLives7.visible = false
		$HBoxContainer2/BossLives8.visible = false
		$HBoxContainer2/BossLives9.visible = false
		$HBoxContainer2/BossLives10.visible = false
		$HBoxContainer2/BossLives11.visible = false
		$HBoxContainer2/BossLives12.visible = false
		$HBoxContainer2/BossLives13.visible = false
		$HBoxContainer2/BossLives14.visible = false
		$HBoxContainer2/BossLives15.visible = false
		$HBoxContainer2/BossLives16.visible = false
		$HBoxContainer2/BossLives17.visible = false
		$HBoxContainer2/BossLives18.visible = false
		$HBoxContainer2/BossLives19.visible = false
		$HBoxContainer2/BossLives20.visible = false

func check_player_lives():
	if Global.player.player_lives == 5:
		$HBoxContainer/PlayerLives1.visible = true
		$HBoxContainer/PlayerLives2.visible = true
		$HBoxContainer/PlayerLives3.visible = true
		$HBoxContainer/PlayerLives4.visible = true
		$HBoxContainer/PlayerLives5.visible = true
	if Global.player.player_lives == 4:
		$HBoxContainer/PlayerLives1.visible = true
		$HBoxContainer/PlayerLives2.visible = true
		$HBoxContainer/PlayerLives3.visible = true
		$HBoxContainer/PlayerLives4.visible = true
		$HBoxContainer/PlayerLives5.visible = false
	if Global.player.player_lives == 3:
		$HBoxContainer/PlayerLives1.visible = true
		$HBoxContainer/PlayerLives2.visible = true
		$HBoxContainer/PlayerLives3.visible = true
		$HBoxContainer/PlayerLives4.visible = false
		$HBoxContainer/PlayerLives5.visible = false
	if Global.player.player_lives == 2:
		$HBoxContainer/PlayerLives1.visible = true
		$HBoxContainer/PlayerLives2.visible = true
		$HBoxContainer/PlayerLives3.visible = false
		$HBoxContainer/PlayerLives4.visible = false
		$HBoxContainer/PlayerLives5.visible = false
	if Global.player.player_lives == 1:
		$HBoxContainer/PlayerLives1.visible = true
		$HBoxContainer/PlayerLives2.visible = false
		$HBoxContainer/PlayerLives3.visible = false
		$HBoxContainer/PlayerLives4.visible = false
		$HBoxContainer/PlayerLives5.visible = false
	if Global.player.player_lives == 0:
		$HBoxContainer/PlayerLives1.visible = false
		$HBoxContainer/PlayerLives2.visible = false
		$HBoxContainer/PlayerLives3.visible = false
		$HBoxContainer/PlayerLives4.visible = false
		$HBoxContainer/PlayerLives5.visible = false

func check_boss_lives():
	if Global.boss.boss_lives == 20:
		$HBoxContainer2/BossLives1.visible = true
		$HBoxContainer2/BossLives2.visible = true
		$HBoxContainer2/BossLives3.visible = true
		$HBoxContainer2/BossLives4.visible = true
		$HBoxContainer2/BossLives5.visible = true
		$HBoxContainer2/BossLives6.visible = true
		$HBoxContainer2/BossLives7.visible = true
		$HBoxContainer2/BossLives8.visible = true
		$HBoxContainer2/BossLives9.visible = true
		$HBoxContainer2/BossLives10.visible = true
		$HBoxContainer2/BossLives11.visible = true
		$HBoxContainer2/BossLives12.visible = true
		$HBoxContainer2/BossLives13.visible = true
		$HBoxContainer2/BossLives14.visible = true
		$HBoxContainer2/BossLives15.visible = true
		$HBoxContainer2/BossLives16.visible = true
		$HBoxContainer2/BossLives17.visible = true
		$HBoxContainer2/BossLives18.visible = true
		$HBoxContainer2/BossLives19.visible = true
		$HBoxContainer2/BossLives20.visible = true
	if Global.boss.boss_lives == 19:
		$HBoxContainer2/BossLives1.visible = false
	if Global.boss.boss_lives == 18:
		$HBoxContainer2/BossLives2.visible = false
	if Global.boss.boss_lives == 17:
		$HBoxContainer2/BossLives3.visible = false
	if Global.boss.boss_lives == 16:
		$HBoxContainer2/BossLives4.visible = false
	if Global.boss.boss_lives == 15:
		$HBoxContainer2/BossLives1.visible = false
		$HBoxContainer2/BossLives2.visible = false
		$HBoxContainer2/BossLives3.visible = false
		$HBoxContainer2/BossLives4.visible = false
		$HBoxContainer2/BossLives5.visible = false
		$HBoxContainer2/BossLives6.visible = true
		$HBoxContainer2/BossLives7.visible = true
		$HBoxContainer2/BossLives8.visible = true
		$HBoxContainer2/BossLives9.visible = true
		$HBoxContainer2/BossLives10.visible = true
		$HBoxContainer2/BossLives11.visible = true
		$HBoxContainer2/BossLives12.visible = true
		$HBoxContainer2/BossLives13.visible = true
		$HBoxContainer2/BossLives14.visible = true
		$HBoxContainer2/BossLives15.visible = true
		$HBoxContainer2/BossLives16.visible = true
		$HBoxContainer2/BossLives17.visible = true
		$HBoxContainer2/BossLives18.visible = true
		$HBoxContainer2/BossLives19.visible = true
		$HBoxContainer2/BossLives20.visible = true
	if Global.boss.boss_lives == 14:
		$HBoxContainer2/BossLives6.visible = false
	if Global.boss.boss_lives == 13:
		$HBoxContainer2/BossLives7.visible = false
	if Global.boss.boss_lives == 12:
		$HBoxContainer2/BossLives8.visible = false
	if Global.boss.boss_lives == 11:
		$HBoxContainer2/BossLives9.visible = false
	if Global.boss.boss_lives == 10:
		$HBoxContainer2/BossLives1.visible = false
		$HBoxContainer2/BossLives2.visible = false
		$HBoxContainer2/BossLives3.visible = false
		$HBoxContainer2/BossLives4.visible = false
		$HBoxContainer2/BossLives5.visible = false
		$HBoxContainer2/BossLives6.visible = false
		$HBoxContainer2/BossLives7.visible = false
		$HBoxContainer2/BossLives8.visible = false
		$HBoxContainer2/BossLives9.visible = false
		$HBoxContainer2/BossLives10.visible = false
		$HBoxContainer2/BossLives11.visible = true
		$HBoxContainer2/BossLives12.visible = true
		$HBoxContainer2/BossLives13.visible = true
		$HBoxContainer2/BossLives14.visible = true
		$HBoxContainer2/BossLives15.visible = true
		$HBoxContainer2/BossLives16.visible = true
		$HBoxContainer2/BossLives17.visible = true
		$HBoxContainer2/BossLives18.visible = true
		$HBoxContainer2/BossLives19.visible = true
		$HBoxContainer2/BossLives20.visible = true
	if Global.boss.boss_lives == 9:
		$HBoxContainer2/BossLives11.visible = false
	if Global.boss.boss_lives == 8:
		$HBoxContainer2/BossLives12.visible = false
	if Global.boss.boss_lives == 7:
		$HBoxContainer2/BossLives13.visible = false
	if Global.boss.boss_lives == 6:
		$HBoxContainer2/BossLives14.visible = false
	if Global.boss.boss_lives == 5:
		$HBoxContainer2/BossLives15.visible = false
	if Global.boss.boss_lives == 4:
		$HBoxContainer2/BossLives16.visible = false
	if Global.boss.boss_lives == 3:
		$HBoxContainer2/BossLives17.visible = false
	if Global.boss.boss_lives == 2:
		$HBoxContainer2/BossLives18.visible = false
	if Global.boss.boss_lives == 1:
		$HBoxContainer2/BossLives19.visible = false
	if Global.boss.boss_lives == 0:
		$HBoxContainer2/BossLives20.visible = false
