extends KinematicBody2D

export (int) var GRAVITY = 120
export (int) var shoot_speed = 120
export (int) var bounce_speed = 200

var player_lives = 3
var shoot_timer = 0

var stun = false
var shield_on = false

const MAX_BEAM_LENGTH = 2000

const UP = Vector2(0,-1)
var velocity = Vector2()

var death_particles = preload("res://Scenes/Player Blood.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.player = self
	$Shield.visible = false
	$Laser.visible = false
	$Bounce.visible = false
	
func _exit_tree():
	Global.player = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	shoot_timer += 1
	
	var mouse_pos = get_local_mouse_position().normalized()
	$RayCast2D.cast_to = mouse_pos * MAX_BEAM_LENGTH

	check_lives()
	velocity = move_and_slide(velocity, UP)

func check_lives():
	if player_lives <= 0:
		$Sprite.visible = false
		
		if Global.node_creation_parent != null:
			Global.node_creation_parent.lose_screen()
			Global.instance_node(death_particles, self.global_position, Global.node_creation_parent)
		
		queue_free()	
		
func bounce():
	$BounceNote.stop()
	var direction = (get_global_mouse_position() - self.global_position).normalized()
	$Bounce.visible = true
	$Bounce.rotation = $RayCast2D.cast_to.angle()
	$BounceNote.play()
	
	shoot_timer = 0
	velocity = -(direction * bounce_speed)
	yield(get_tree().create_timer(.1), "timeout")
	$Bounce.visible = false
		
func shoot():
	$ShootNote.stop()
	var direction = (get_global_mouse_position() - self.global_position).normalized()
	$ShootNote.play()
	$Laser.visible = true
	$Laser.rotation = $RayCast2D.cast_to.angle()
	
	if $RayCast2D.is_colliding():
		$EndPosition.global_position = $RayCast2D.get_collision_point()
	else:
		$EndPosition.global_position = $RayCast2D.cast_to
		
	$Laser.region_rect.end.x = $EndPosition.position.length()
	shoot_timer = 0
	
	velocity = -(direction * shoot_speed)
	yield(get_tree().create_timer(.1), "timeout")
	$Laser.visible = false
	
	check_if_laser_hit_enemy()
	
func deploy_shields():
	shield_on = true
	$Shield.visible = true
	$ShieldTimer.start()

func check_if_laser_hit_enemy():
	if $RayCast2D.is_colliding() and $RayCast2D.get_collider().has_method("destroy"):
		$RayCast2D.get_collider().destroy()

func invincible():
	stun = true

func _on_StunTimer_timeout():
	$Sprite.modulate = Color("ffffff")
	stun = false

func _on_HitBox_area_entered(area):
	if area.is_in_group("Shields") and shield_on == false:
		area.get_parent().queue_free()
		deploy_shields()
		
	if area.is_in_group("Healths"):
		area.get_parent().queue_free()
		$HealthParticles.emitting = true
		$HealthTimer.start()
		
		if player_lives < 5:
			player_lives += 1

func _on_HitBox_body_entered(body):
	if body.is_in_group("Enemies") or body.is_in_group("Bosses"):
		if stun == false:
			player_lives -= 1
			stun = true
			velocity = -velocity
			$Sprite.modulate = Color("ff00ff")
			$StunTimer.start()

func _on_ShieldArea_body_entered(body):
	if body.is_in_group("Enemies") and shield_on == true:
		body.destroy()

func _on_ShieldTimer_timeout():
	shield_on = false
	$Shield.visible = false

func _on_HealthTimer_timeout():
	$HealthParticles.emitting = false
